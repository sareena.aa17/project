import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


import 'package:android/products.dart';
import 'package:android/details.dart';
import 'package:android/create.dart';
import 'dart:ui';
import 'package:android/aboutus.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() {
  
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Icon(
      FontAwesomeIcons.star, // ตัวอย่าง: ใช้ FontAwesomeIcons.star
      color: Colors.blue,
      size: 30,
    );

// หรือใช้ในปุ่ม
    IconButton(
      icon: Icon(FontAwesomeIcons.heart),
      onPressed: () {
        // ทำอะไรก็ตามเมื่อปุ่มถูกคลิก
      },
    );
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: Home(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Home extends StatefulWidget {


  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  late Future<List<Products>> products;


  @override
  void initState() {
    super.initState();
    products = getProductsList();
  }

  Future<List<Products>> getProductsList() async {
    String url = "http://10.0.2.2/android/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productsFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load products');
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Products List'),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: IconButton(
              icon: const Icon(Icons.people_alt_outlined),
              tooltip: 'About Us',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AboutUs()),
                ).then((value){
                  //getAllData();
                  setState(() {

                  });
                });
              },
            ),
          ),
        ],
      ),
      body: Center(
        child: FutureBuilder<List<Products>>(
          future: products,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return CircularProgressIndicator();
            // Render student lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    leading: Icon(FontAwesomeIcons.heart),
                    title: Text(
                      data.name,
                      style: TextStyle(fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Details(products: data)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Create()),
          );
        },
      ),
    );
  }
}
