import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:android/products.dart';

class Edit extends StatefulWidget {
  final Products products;

  Edit({required this.products});

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  // This is for text to edit
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  // Http post request
  Future<http.Response> editProducts() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/update_product.php"),
      body: {
        "pid": widget.products.pid.toString(),
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text,
      },
    );
  }

  void _onConfirm(BuildContext context) async {
    await editProducts();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    nameController = TextEditingController(text: widget.products.name);
    priceController = TextEditingController(text: widget.products.price.toString());
    descriptionController = TextEditingController(text: widget.products.description.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit products"),
      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton.icon(
          label: Text('Save'),
          icon: Icon(Icons.save),
          style: ElevatedButton.styleFrom(
            primary: Colors.orange,
            onPrimary: Colors.white,
          ),
          onPressed: () {
            _onConfirm(context);
          },
        ),
      ),
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Container(
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    labelText: "Products Name:",
                    hintText: "Enter products name",
                  ),
                )),
            Container(
                child: TextField(
                  controller: priceController,
                  decoration: InputDecoration(
                    labelText: "price:",
                    hintText: "Enter product price",
                  ),
                )),
            Container(
                child: TextField(
                  controller: descriptionController,
                  decoration: InputDecoration(
                    labelText: "Products description:",
                    hintText: "Enter Products description",
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
